'''
@file Lab5Ex.py
@author Kai Quizon, adapted from Dr. Wade
@brief This code, run on a Nucleo, accepts a bluetooth transmitted value and changes that status of an LED.
'''

import pyb
from pyb import UART

## Initialize LED pin
pinA5 = pyb.Pin(pyb.Pin.cpu.A5)

## Set up UART for communication with bluetooth attena
serial = UART(3,9600)


while True:
    if serial.any() != 0:
        val = int(serial.readline())
        if val == 0:
            print('The lights are out sir.')
            pinA5.low()
            val = None
        elif val == 1:
            print('The flame burns brightly my liege.')
            pinA5.high()
            val = None
        else:
            print('1s and 0s only, peasant >:(')
            val = None
