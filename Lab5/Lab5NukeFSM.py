'''
@file Lab5NukeFSM.py
@author Kai Quizon
@brief This FSM runs on the Nucleo awaiting an input from a bluetooth connected User Interface.
@details This FSM awaits input from a bluetooth connected user interface run
         on the User's smart device. The User has three input options on their
         device. They may either turn the LED off, turn the LED on, or send
         a numeric value to the Nucleo. This numeric value is then interpreted
         by the below FSM and the LED is blinked at a frequency equal to the
         numeric value sent. The FSM below is designed for values ranging
         from 0-10. Values significantly outside this range may appear as a
         solidly lit LED. The state diagram may be viewed below 
         @image html Lab5Nuke.png width=60in
'''

import pyb
from pyb import UART
import utime

class Lab5Nuke:
    '''
    @brief      A finite state machine to interpret user inputs from the bluetooth module.
    @details    This class implements a finite state machine to control an LED
                in three different cases by interpreting User inputs sent via
                Bluetooth and read using the DSD Tech HM-19 Bluetooth attenna.
    '''
        ## Constant defining State 0 - Initialization
    S0_INIT         = 0    
    
    ## Constant defining State 1 - Wait for Command
    S1_WAIT         = 1
    
    ## Constant defining State 3 - LED On
    S3_On         = 2    
    
    ## Constant defining State 4 - LED Off
    S4_Off         = 3
    
    
    def __init__(self, interval):
        '''
        @param interval The time separating each iteration of the task.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Interval setup
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## UART initialization
        self.serial = UART(3,9600)
        
        ## LED Pin Initialization
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        
    def run(self, boolflag):
        '''
        Runs one iteration of the class.
        '''
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## State 0
            if self.state == self.S0_INIT:
                self.pinA5.low() #Ensure the LED always begins OFF.
                self.transitionTo(self.S1_WAIT)
                #print('Initialized')   ## Troubleshooting
                
            ## State 1    
            if self.state == self.S1_WAIT:
                if self.serial.any() != 0:
                    self.init = self.serial.read()
                    #print(self.init)  ## Troubleshooting
                    
                    ## User Pushes ON Button
                    if self.init == b'103':
                        self.pinA5.high()
                        self.init = None
                        print('LED On')
                    
                    ## User Pushes OFF Button
                    elif self.init == b'115':
                        self.pinA5.low()
                        self.init = None
                        print('LED Off')
                        
                    ## User Inputs a Numeric Value
                    else:
                        nextint = 0
                        interval = int((1/int(self.init))*1E6)
                        self.transitionTo(self.S3_On)
                        # print('LED blinking at {} Hz.'.format(self.init)) ## (Troubleshooting)
                        self.init = None
                        ## Baby Finite State Machine AKA State 1 is Pregnant
                        while self.serial.any() == 0:
                            localtime = utime.ticks_us()
                            if utime.ticks_diff(localtime, nextint) >= 0:
                                if self.state == self.S3_On:
                                    self.pinA5.high()
                                    nextint = utime.ticks_add(localtime,interval)
                                    self.transitionTo(self.S4_Off)
                                elif self.state == self.S4_Off:
                                    self.pinA5.low()
                                    nextint = utime.ticks_add(localtime,interval)
                                    self.transitionTo(self.S3_On)
                                    
                        self.transitionTo(self.S1_WAIT)
                                    
                    self.next_time = utime.ticks_add(self.next_time,self.interval)
                        

            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState
            
            
            
            