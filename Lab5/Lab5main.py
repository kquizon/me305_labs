'''
@file Lab5main.py
@author Kai Quizon
@brief This file continously runs the Lab5NukeFSM when run on the Nucleo.
'''

import pyb
from pyb import UART
import utime
from Lab5NukeFSM import Lab5Nuke

task = Lab5Nuke(1_00)

while True:
    task.run(boolflag)