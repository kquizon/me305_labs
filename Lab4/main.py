'''
@file main.py
@brief This file contains code to execute sensor data collection locally on the Nucleo when requested by \ref PCUI.py
@author Kai Quizon
@details 
'''

from pyb import UART
import utime
from Encoder import Encoder
from Lab4EncoderTask import Lab4EncoderTask

Encoder = Encoder('A6','A7',3)

task1 = Lab4EncoderTask(2_00000, 'A6', 'A7', 3, Encoder)

while True:
    task1.run()