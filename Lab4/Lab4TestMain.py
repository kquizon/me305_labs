'''
@file Lab4TestMain.py
@author Kai Quizon
@brief The main test file for running Lab 4 test code on the nucleo.
'''

import utime
from pyb import UART
from Encoder import Encoder
from Lab4Test import Lab4Test


task = Lab4Test(2_00000, 'A6', 'A7', 3, Encoder)

while True:
    task.run()