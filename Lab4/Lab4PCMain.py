'''
@file Lab4PCMain.py
@author Kai Quizon
@brief The main file running the PC User Interface as a task
'''

import serial
import time
from matplotlib import pyplot
import csv
from PCUI import PCUI

task = PCUI(1)

while True:
    task.run()