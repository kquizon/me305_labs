'''
@file PCUI.py
@brief This file contains the PC User Interface to read data from a Nucleo Sensor
@author Kai Quizon
@details This file contains a PC User Interface that accepts two commands to
         initiate and manipulate data collection on the Nucleo L476 Dev board.
         A user input of g initiates data collection. A user input of s stops
         data collection. This interaction is handled using the pySerial
         module on the PC side and the pyb UART module on the Nucleo side.
         When data collection has completed or been terminated, data is sent 
         to the PC, parsed into a .CSV file and plotted. A sample plot output 
         is shown below @image html Lab4OutputSample.png "Sample Output" width=600in
'''

import serial
import time
from matplotlib import pyplot
import csv
import numpy as np
#import nukeshares


#initiate serial port
ser = serial.Serial(port='COM10', baudrate = 115273,timeout=1)

class PCUI:
    '''
    PC User Interface task.
    
    An object of this class initializes and runs the user interface corresponding
    to Nucleo L476 Dev Board sensor data collection. Allowable user inputs are
    g for beginning data collection and s for terminating data collection.
    COM ports, baudrate, and timeouts are pre-perscribed.
    '''
    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_DataCollection   = 1
    
    def __init__(self, interval):
        '''
        @param interval Interval describes the interval at which the task will loop.
        '''
        
        ## Serial port
        self.ser = ser
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        self.PCData = []
        
            
        print('''
              Welcome. This terminal will allow you to control the data collection
              process of the Nucleo L476 Dev Board. Acceptable User Inputs are:
                  g: initiates data collection
                  s: terminates data collection
              Please note that user inputs are case sensitive. Also, data 
              collection will automatically terminate after 10 seconds if no
              valid user input is detected.
              ''')
    def run(self):
        '''
        Runs one iteration of the task.
        '''
        
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                self.cmd = input('Enter g to initiate data collection: ')
                if self.cmd == 'g':
                    #Send User input to nucleo
                    self.ser.write(str(self.cmd).encode('ascii'))
                    #Clear Command
                    self.cmd = None
                    self.transitionTo(self.S1_DataCollection)
                    collecttime = time.time()
                elif self.cmd != 'g':
                    print('Please input a valid command (g): ')
                else:
                    pass #error handling
                    
            elif(self.state == self.S1_DataCollection):     
                self.term = input('To terminate, press s: ')
                #Handle Early Termination
                if self.term == 's' or time.time-collecttime >= 10:
                    if self.term == 's':
                        #Send input
                        self.ser.write(str(self.term).encode('ascii'))
                        #Clear Command
                        self.term = None
                        
                    #Initiate data reading
                    data_list = list(ser.readline().decode('ascii').split('MM'))
                    PCData = [m.strip('[]').split(', ') for m in data_list]
                    time_stamp = [int(t) for t in PCData[0]]
                    enc_pos = [int(t) for t in PCData[1]]
                    #time_refined = [float(t) for t in PCData[2]]
                
                    
                    #print(enc_pos) #Debugging
                        
                    #Plotting Data
                    pyplot.plot(time_stamp, enc_pos)
                    pyplot.xlabel('Time (s)')
                    pyplot.ylabel('Encoder Position')
                    filename = 'Encoder Output'+time.strftime('%Y%m%d-%H%M%S')
                    
                    #Creating CSV File
                    np.savetxt(filename, [time_stamp, enc_pos], delimiter = ', ')
                    quit()
                        
                elif(self.term != 's'):
                    self.cmd = input('Please enter a valid command(Data collection intialization is unavaible during collection): ')
                else:
                    pass
                    
            self.next_time += self.interval

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState
        