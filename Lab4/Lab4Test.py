'''
@file Lab4Test.py
@brief This file is a test file that runs the equivalent code only locally on the Nucleo.
@author Kai Quizon
'''

import utime
from pyb import UART
from Encoder import Encoder
import pyb

class Lab4Test:
    '''
    @brief A test class that runs locally on the Nucleo for testing Encoder code.
    
    An object of this class reads values from a motor encoder or updates them as dictated by the user inputs.   
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1
    
    S3 = 2

    def __init__(self, interval, pin1, pin2, timer, Encoder):
        '''
        Creates an Encoder task object.
        @param interval An integer number of microseconds between desired runs of the task
        @param pin1 The first pin on the Nucleo board the encoder is connected to (ex: 'A6')
        @param pin2 The second pin on the Nucleo board the encoder is connected to (ex: 'A7')
        @param timer The appropriate timer corresponding to the selected pins, as demanded by the Nucleo documentation
        @param Encoder The Encoder class on which the Encoder task is to operate
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        self.data_int = 0

        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        
        self.Encoder = Encoder #store class copy
        
        self.enc_pos = 0 #initialize encoder at zero
        self.enc_prev = 0 #initialize previous encoder value at zero
        self.tim = pyb.Timer(3)
        
        self.serial = UART(2)
        
        self.DataCollection = []
        self.Time = []
        self.Time_stamp = []

    def run(self):
        '''
        Runs one iteration of the task
        '''
        
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):


                if utime.ticks_diff(self.curr_time, self.start_time) >= 10000000:
                    for n in range(len(self.Time_stamp)):
                        self.Time.append((self.Time_stamp[n] - self.Time_stamp[0])/1E6)
                    print('Terminating Data Collection')
                    print(self.DataCollection)
                    print(self.Time)
                    self.transitionTo(self.S3)
                    
                else:
                     Encoder.update(self)
                     self.DataCollection.append(Encoder.get_position(self))
                     self.Time_stamp.append(utime.ticks_diff(self.curr_time, self.start_time))
                     

                     
            elif(self.state == self.S3):
                quit()
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState
