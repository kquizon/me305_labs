'''
@file Lab4EncoderTask.py
@author Kai Quizon
@brief This task runs the desired encoder functions on the Nucleo
@details This task calls the Encoder class from Lab 3 at the required intervals
         to produce a positional graph of the encoder across the desired 10
         second or shorter interval. This task also relies on the UART module 
         for serial communication and the utime module for timing. 
'''


import utime
from Encoder import Encoder
from pyb import UART
import pyb

class Lab4EncoderTask:
    '''
    @brief Encoder task. Handles interaction with hardware.
    
    An object of this class reads values from a motor encoder or updates them as dictated by the user inputs.   
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1

    def __init__(self, interval, pin1, pin2, timer, Encoder):
        '''
        Creates an Encoder task object.
        @param interval An integer number of microseconds between desired runs of the task
        @param pin1 The first pin on the Nucleo board the encoder is connected to (ex: 'A6')
        @param pin2 The second pin on the Nucleo board the encoder is connected to (ex: 'A7')
        @param timer The appropriate timer corresponding to the selected pins, as demanded by the Nucleo documentation
        @param Encoder The Encoder class on which the Encoder task is to operate
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        self.data_int = 0

        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        
        self.Encoder = Encoder #store class copy
        
        self.enc_pos = 0 #initialize encoder at zero
        self.enc_prev = 0 #initialize previous encoder value at zero
        self.tim = pyb.Timer(3)
        
        self.serial = UART(2)
        
        self.DataCollection = []
        self.Time = []
        self.Time_stamp = []

    def run(self):
        '''
        Runs one iteration of the task
        '''
        
        if self.serial.any() != 0:
            self.init = self.serial.readchar()
            #print('Read a character')
            if self.init == 103:
                self.init == None
                while True:
                    self.curr_time = utime.ticks_us()
                    if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                        
                        Encoder.update(self)
                        
                        if(self.state == self.S0_INIT):
                            # Run State 0 Code
                            self.transitionTo(self.S1_WAIT_FOR_CMD)
                        
                        elif(self.state == self.S1_WAIT_FOR_CMD):
            

                            if self.serial.readchar() == 115 or utime.ticks_diff(self.curr_time, self.start_time) >= 10000000:
                                self.transitionTo(self.S0_INIT)
                                for n in range(len(self.Time_stamp)):
                                    self.Time.append((self.Time_stamp[n] - self.Time_stamp[0])/1E6)
                                #print('Terminating Data Collection')
                                self.cmd = None
                                #Then begin sending data to PC
                                self.serial.write(str(self.Time_stamp)+'MM'+str(self.DataCollection)+'MM'+str(self.Time))
                                
                            else:
                                Encoder.update(self)
                                self.DataCollection.append(Encoder.get_position(self))
                                self.Time_stamp.append(utime.ticks_diff(self.curr_time, self.start_time))
                                #print('Appended to array')
                     
                        else:
                            # Invalid state code (error handling)
                            pass
                        
                        self.runs += 1
                        
                        # Specifying the next time the task will run
                        self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState
