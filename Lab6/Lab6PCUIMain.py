'''
@file Lab6PCUIMain.py
@brief The main file calling the \ref Lab6PCUI.py task on the python kernal.
@author Kai Quizon
'''

import serial
import time
from matplotlib import pyplot
import numpy as np
from Lab6PCUI import Lab6PCUI

task = Lab6PCUI(0.1, 0.08, 1000)

## Run Task
while True:
    task.run()

