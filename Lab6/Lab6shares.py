'''
@file Lab6shares.py
@brief This file acts as the communicator between the control and execution files for Lab 6
@author Kai Quizon
'''


## State ref_vel
ref_vel = None

## Initiation Variable
init = None

## Preallocate Data Arrays
time = []
velocity = []
ref = []


