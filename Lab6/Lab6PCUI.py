'''
@file Lab6PCUI.py
@brief This file contains a class governing the PC User Interface for Lab 6 Motor Controller.
@author Kai Quizon
'''

import serial
import time
from matplotlib import pyplot
import numpy as np

#initiate serial port
ser = serial.Serial(port='COM10', baudrate = 115273,timeout=1)

class Lab6PCUI:
    '''
    PC User Interface task.
    
    An object of this class initializes and runs the user interface corresponding
    to Nucleo L476 Dev Board sensor data collection. The only user interface
    is the desired proportionality constant for closed loop feedback control.
    COM ports, baudrate, and timeouts are pre-perscribed.
    '''
    
    ## Distribution
    S0_Distribution             = 0
    
    ## Data Processing (State 1)
    S1_Processing               = 1
    

    
    def __init__(self, interval, Kp, ref_vel):
        '''
        @param interval Interval describes the interval at which the task will loop.
        '''
        
        ## Serial port
        self.ser = ser
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_Distribution
        
            
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## Initial control parameters
        self.Kp = Kp
        self.ref_vel = ref_vel
        
        
        
        
    def run(self):
        '''
        Runs one iteration of the task.
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            ## Check control parameters and send to Nuke
            if(self.state == self.S0_Distribution):
               
                ## Handle invalid Kps
                
                ## Write control parameters to serial port
                Kptransmit = str(self.Kp)+'\n'
                self.ser.write(Kptransmit.encode())
                self.ser.write(str(self.ref_vel).encode())
                
                
                ## Transition to waiting/processing state
                self.transitionTo(self.S1_Processing)
                
                
            ## Wait for data to be sent back, process, and plot
            if(self.state == self.S1_Processing):
                self.term = input('To terminate, press s: ')
                #Handle Early Termination
                if self.term == 's':
                    #Send stop command to nucleo
                    self.ser.write(str(self.term).encode('ascii'))
                    #Clear Command
                    self.term = None
                    
                    ## Begin Data Transfer
                    data_list = list(ser.readline().decode('ascii').split('MM'))
                    PCData = [m.strip().strip('[]').split(', ') for m in data_list]
                    time_stamp = [float(t) for t in PCData[0]]
                    motor_velocity = [float(t) for t in PCData[1]]
                    #print(motor_velocity)
                    vel_input = [float(t) for t in PCData[2]]
                    
                    ## Plot Motor Response
                    pyplot.plot(time_stamp, motor_velocity, 'r', time_stamp, vel_input, 'g--')
                    pyplot.xlabel('Time (s)')
                    pyplot.ylabel('Motor Velocity (rpm)')
                    pyplot.title('Motor Velocity Over Time Compared to Input Velocity')
                    pyplot.text(0,0,'Kp = '+str(self.Kp))
                    
                    
                    ## Creating CSV File
                    #filename = 'MotorVelocity'+time.strftime('%Y%m%d-%H%M%S')
                    #np.savetxt(filename, [time_stamp, motor_velocity], delimiter = ', ')
                    quit()
                        
                elif(self.term != 's'):
                    self.cmd = input('Please enter a valid command: ')

                                
                
            self.next_time += self.interval    
                
    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState
                      
                
    
    
    
    
    
    
    
    
    
    