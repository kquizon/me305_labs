'''
@file Lab6ExecutorTask.py
@brief This file controls the drivers necessary for operating the motor and reading its velocity.
@author Kai Quizon
'''

import pyb
from Encoder import Encoder
from MotorDriver import MotorDriver
from ClosedLoop import ClosedLoopController
import Lab6shares
import utime

class Lab6executor:
    '''
    @brief This class runs locally on the Nucleo to test \ref Lab6Executor.py.
    
    This class interacts with \ref Lab6ControlTask.py and therefore 
    \ref Lab6PCUI.py to control a motor with the Nucleo L476 Dev Board. 
    The class calls a closed loop control and executes the control scheme via
    PWM. The control class also utilizes data from the motor encoder.
    '''
    
    def __init__(self, interval):
        '''
        Create an executor tasks.
        '''
        
        ## Store class copies of imported modules
        self.Encoder = Encoder('B6', 'B7', 4)
        self.MotorDriver = MotorDriver('A15','B4','B5',3)
        self.ClosedLoop = ClosedLoopController(0.75)
        
        ## Update Encoder at initial position
        self.Encoder.update()
        
        ## Define interval for the task to run. Also implicitly defines 
        ## delta t for velocity calculations.
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Enable Motor
        self.MotorDriver.enable()
        
    def run(self):
        '''
        Runs one iteration of the task.
        '''
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if Lab6shares.init == None:
                ## Read Encoder Value and Convert to RPM
                self.Encoder.update()
                PPR = 4000
                interdelta = self.Encoder.get_delta()/PPR
                dividerpre = self.interval*10E-6
                divider = dividerpre*1/60
                vel_real = interdelta/divider # Must convert self.interval to minutes to get calc in RPM
                
                ## Write relevant data to shares file
                #Lab6shares.velocity.append(vel_real)
                #Lab6shares.ref.append(Lab6shares.ref_vel)
                #Lab6shares.time.append(self.curr_time-self.start_time)
                
                ## Calculate Control Loop Gain
                L = self.ClosedLoop.update(100, vel_real)
                vel_real = None
                
                ## Change motor duty cycle based on Control Loop Output
                self.MotorDriver.set_duty(L)
                
                # Specifying the next time the task will run
                self.next_time = utime.ticks_add(self.next_time, self.interval)
            else:
                pass
        
    

