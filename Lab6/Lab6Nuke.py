'''
@file Lab6Nuke.py
@brief One file handling serial communication and hardware control onboard the Nucleo. Loaded to \ref main.py
@details Based on the following Finite State Machine model @image html Lab6NukeFSM.png width=600in
@author Kai Quizon
'''

import pyb
from Encoder import Encoder
from MotorDriver import MotorDriver
from ClosedLoop import ClosedLoopController
import utime
from pyb import UART


class Lab6Nuke:
    '''
    @brief This class controls the motor and writes relevant motor data to the serial port.
    
    This class interacts with \ref Lab6ControlTask.py and therefore 
    \ref Lab6PCUI.py to control a motor with the Nucleo L476 Dev Board. 
    The class calls a closed loop control and executes the control scheme via
    PWM. The control class also utilizes data from the motor encoder.
    '''
    
    ## Define States
    S0_init    = 0
    
    S1_Running = 1
    
    S2_DONE    = 2
    
    def __init__(self, interval):
        '''
        Create an executor tasks.
        '''
        
        ## Initiate Serial Port Communication
        self.ser = UART(2)
        
        ## Store class copies of imported modules
        self.Encoder = Encoder('B6', 'B7', 4)
        self.MotorDriver = MotorDriver('A15','B4','B5',3)
        self.ClosedLoop = ClosedLoopController
        
        
        ## Update Encoder at initial position
        self.Encoder.update()
        
        ## Define interval for the task to run. Also implicitly defines 
        ## delta t for velocity calculations.
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Enable Motor
        self.MotorDriver.enable()
        self.MotorDriver.set_duty(0)
        
        ## Preallocate Arrays
        self.velocity = []
        self.time = []
        self.ref = []
        
        ## Initial State
        self.state = self.S0_init
        
    def run(self):
        '''
        Runs one iteration of the task.
        '''
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            self.Encoder.update()
            if self.state == self.S0_init:
                if self.ser.any() != 0:
                    ## Read Serial input and assign reference vel
                    self.Kp = float(self.ser.readline())
                    self.ref_vel = int(self.ser.readline())
                    #print(self.ref_vel)
                    
                    ## Initiate Closed Loop
                    self.ClosedLoop = ClosedLoopController(self.Kp)
                    
                    self.transitionTo(self.S1_Running)
                    
            if self.state == self.S1_Running:
                if self.ser.any() == 0:
                    ## Read Encoder Value and Convert to RPM
                    PPR = 4000
                    interdelta = self.Encoder.get_delta()/PPR
                    dividerpre = self.interval*1E-6
                    divider = dividerpre/60
                    vel_real = interdelta/divider # Must convert self.interval to minutes to get calc in RPM
                    
                    ## Write relevant data to shares file
                    self.velocity.append(vel_real)
                    self.ref.append(self.ref_vel)
                    self.time.append((self.curr_time-self.start_time)/1E6)
                    
                    ## Calculate Control Loop Gain
                    L = self.ClosedLoop.update(self.ref_vel, vel_real)
                    vel_real = None
                    
                    ## Change motor duty cycle based on Control Loop Output
                    self.MotorDriver.set_duty(L)
                    
                else:
                    self.MotorDriver.set_duty(0)
                    self.ser.write(str(self.time)+'MM'+str(self.velocity)+'MM'+str(self.ref))
                    self.transitionTo(self.S2_DONE)
                    
            if self.state == self.S2_DONE:
                pass
                    
                
                
                
                
            
            else:
                pass
                
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState