'''
@file Lab6ControlTask.py
@brief This task controls the user inputs and passes relevant, formatted data to the execution task.
@author Kai Quizon
'''

import pyb
import Lab6shares
from pyb import UART as UFART
import utime



class Lab6Control:
    '''
    @brief This test code runs locally on the Nucleo with the same goals as \ref Lab6ControlTask.py.
    
    This class interacts with the Lab 6 PC User Interface to receive user 
    inputs, format control data, and send relevant, formatted data to the
    execution task.
    '''
    
    ## Initializing (State 0)
    S0_INIT             = 0
    
    ## Controlling Execution Tasks (State 1)
    S1_Control               = 1
    
    def __init__(self, interval):
        '''
        @param interval Interval describes the interval at which the task will loop.
        '''
        
        ## Define Interval
        self.interval = interval
        
        ## Initiate Serial Port Communication
        self.ser = UFART(2)
        
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        
    def run(self):
        
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            ## State 0: Initialization
            if self.state == self.S0_INIT:
                ## Check for waiting characters in serial port
                #if self.serial.any() != 0:
                    ## Read characters from serial port in order from PCUI
                    self.vel_ref = 100
                    Lab6shares.vel_ref = self.vel_ref

            else:
                # Invalid state code (error handling)
                pass
                        

                        
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
                    
                    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState
            
        