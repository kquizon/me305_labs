'''
@file main.py
@brief The program the Nucleo will execute on startup
@details Calls the Lab6ControlTask and the Lab6ExecutorTask, which in turn
         calls the MotorDriver, ClosedLoop, and Encoder classes utilizes
         Lab6shares file to share variables between the Control and Execution
         tasks.
@author Kai Quizon
'''

import pyb
import Lab6shares
from pyb import UART as UFART
import utime
from Encoder import Encoder
from MotorDriver import MotorDriver
from ClosedLoop import ClosedLoopController
from Lab6ControlTask import Lab6Control
from Lab6ExecutorTaskTEST import Lab6executor

#taskControl = Lab6Control(2_0)
taskExecutor = Lab6executor(2_0000)


## Execute tasks cooperatively
while True:
    #taskControl.run()
    taskExecutor.run()
