'''
@file main.py
@brief The program the Nucleo will execute on startup
@details Calls \ref Lab7Nuke.py, which in turn calls the \ref MotorDriver.py, \ref ClosedLoop.py, 
         and \ref Encoder.py classes.
@author Kai Quizon
'''

import pyb
from pyb import UART as UFART
import utime
from Encoder import Encoder
from MotorDriver import MotorDriver
from ClosedLoop import ClosedLoopController
from Lab7Nuke import Lab7Nuke

task = Lab7Nuke(2_0000)



## Execute tasks cooperatively
while True:
    task.run()
