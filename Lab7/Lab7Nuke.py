'''
@file Lab7Nuke.py
@brief One file handling serial communication and hardware control onboard the Nucleo. Loaded to \ref main.py
@details Based on the following Finite State Machine model @image html Lab7NukeFSM.png width=600in
@author Kai Quizon
'''

import pyb
from Encoder import Encoder
from MotorDriver import MotorDriver
from ClosedLoop import ClosedLoopController
import utime
from pyb import UART
import array


class Lab7Nuke:
    '''
    @brief This class reads the csv, controls the motor, and writes relevant motor data to the serial port.
    
    @details This class interacts with \ref MotorDriver.py, \ref Encoder.py, and \ref ClosedLoop.py  
    to \ref Lab6PCUI.py to control a motor with the Nucleo L476 Dev Board. 
    The class calls a closed loop control and executes the control scheme via
    PWM. The control class also utilizes data from the motor encoder.
    '''
    
    ## Define States
    S0_init    = 0
    
    S1_Running = 1
    
    S2_DONE    = 2
    
    def __init__(self, interval):
        '''
        @brief Create an executor task for controlling motor on profile.
        '''
        
        ## Initiate Serial Port Communication
        self.ser = UART(2)
        self.array = array
        
        ## Store class copies of imported modules
        self.Encoder = Encoder('B6', 'B7', 4)
        self.MotorDriver = MotorDriver('A15','B4','B5',3)
        self.ClosedLoop = ClosedLoopController
        
        
        ## Update Encoder at initial position
        self.Encoder.update()
        
        ## Define interval for the task to run. Also implicitly defines 
        ## delta t for velocity calculations.
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Enable Motor
        self.MotorDriver.enable()
        self.MotorDriver.set_duty(0)
        
        ## Preallocate Arrays
        self.velocity = self.array.array('f', [0])
        self.position = self.array.array('f', [0])
        self.time = self.array.array('f', [0])
        self.J_calcs = self.array.array('f', [0])
        self.trunc_time = self.array.array('f', [0])
        self.trunc_vel = self.array.array('f', [0])
        self.trunc_pos = self.array.array('f', [0])

        ## Initial State
        self.state = self.S0_init
        
    def run(self):
        '''
        Runs one iteration of the task.
        '''
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            self.Encoder.update()
            
            if self.state == self.S0_init:
                if self.ser.any() != 0:
                    ## Read Serial input and assign reference vel
                    self.Kp = self.ser.readline()
                    ## Initiate Closed Loop
                    self.ClosedLoop = ClosedLoopController(self.Kp)
                        
                    ## Open and Read csv file
                    ref_data = open('lab7_reference.csv')
                    while True:
                        line = ref_data.readline()
                        
                        if line == '':
                            break
                        
                        else:
                            (t,v,x) = line.strip().split(',')
                            if float(t)%(self.interval*1E-6) <1E-4:
                                self.trunc_time.append(float(t))
                                self.trunc_vel.append(float(v))
                                self.trunc_pos.append(float(x))
                        

                        

                self.transitionTo(self.S1_Running)
                    
            if self.state == self.S1_Running:
                
                
                runs = 0
                while len(self.time) <= len(self.trunc_time):
                    ## Read Encoder Value and Convert to RPM
                    PPR = 4000
                    interdelta = self.Encoder.get_delta()/PPR
                    dividerpre = self.interval*1E-6
                    divider = dividerpre/60
                    vel_real = interdelta/divider # Must convert self.interval to minutes to get calc in RPM
                    
                    ## Get Encoder Position (in degrees)
                    PPD = PPR/360 #Pulses per degree
                    pos_real = self.Encoder.get_position()/PPD
                    
                    
                    ## Write relevant data to arrays
                    self.velocity.append(vel_real)
                    self.position.append(pos_real)
                    self.time.append((self.curr_time-self.start_time)/1E6)
                    
                    
                    ## Calculate Control Loop Gain
                    refvel = self.trunc_vel[runs]
                    L = self.ClosedLoop.update(refvel, vel_real)
                    refvel = None
                    
                    ## Change motor duty cycle based on Control Loop Output
                    self.MotorDriver.set_duty(L)
                    
                    ## Calculate J
                    vel_diff = self.trunc_vel[runs] - self.velocity[runs]
                    pos_diff = self.trunc_pos[runs] - self.position[runs]
                    J = vel_diff**2 + pos_diff**2
                    self.J_calcs.append(J)
                    vel_real = None
                    pos_real = None
                    
                    ## Increment runs
                    runs += 1
                    
                self.MotorDriver.set_duty(0)
                ## Calculate Final J
                J_True = sum(self.J_calcs)/len(self.J_calcs)
                
                ## Write all relevant data as individual lines
                self.ser.write(str(J_True)+'\n')
                self.ser.write(str(self.time)+'\n')
                self.ser.write(str(self.velocity)+'\n')
                self.ser.write(str(self.position)+'\n')
                self.ser.write(str(self.trunc_vel)+'\n')
                self.ser.write(str(self.trunc_pos)+'\n')
                self.transitionTo(self.S2_DONE)
                    
            if self.state == self.S2_DONE:
                pass
            
            else:
                pass
                
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState