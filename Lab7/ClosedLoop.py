'''
@file ClosedLoop.py
@brief A class that implements a closed loop feedback system with a user set proportionality constant.
@author Kai Quizon
'''


class ClosedLoopController:
    '''
     @brief A class that implements a closed loop feedback system with a user set proportionality constant.
     
    '''
    
    def __init__(self, Kp_init):
        '''
        @brief Creates a ClosedLoopController Object and initiates feedback system
        @param Kp_init The initial porportionality constant.
        @param val_ref The desired value of the system to be controlled.
        '''
        self.Kp_init = Kp_init
       
        
    def update(self, val_true, val_ref):
        '''
        @brief Calculates the required duty cycle based on reference and true values.
        @param val_true The true value as read from hardware
        '''
        deltar = val_ref-val_true
        self.Actval = self.Kp_init*deltar
        ## Handle duty cycles greater than 100%
        if abs(self.Actval) > 100:
            if self.Actval > 0:
                self.Actval = 100
            else:
                self.Actval = -100
        return self.Actval
                
    def set_Kp(self, newKp):
        '''
        @brief Sets a new proportionality constant
        @param newKp The new desired proportionality constant.
        '''
        
        self.Kp_init = newKp
        
    def get_Kp(self):
        '''
        @brief Returns the current proportionality constant.
        '''
        return self.Kp_init
        
    
        
