'''
@file MotorDriver.py
@author Kai Quizon (adapted from code from Charlie Revfam)
@brief A class to control a motor object with a PCB interface to the Nucleo L476 Dev Board
'''

import pyb

class MotorDriver:
    '''
    @brief This class controls a motor object for the ME 3/405 PCB interfacing with the Nucleo L476 Dev board.
    '''
    
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        '''
        Creates a motor driver by initializing GPIO pins and turning the motor
        off for safety.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin. MUST be input as a string.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1. MUST be input as a string.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2. MUST be input as a string.
        @param timer        A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.

        '''
        

        self.pinA15 = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        pinB4 = pyb.Pin(IN1_pin)
        pinB5 = pyb.Pin(IN2_pin)
        tim = pyb.Timer(timer, freq=20000)
        self.ch1 = tim.channel(1, pyb.Timer.PWM, pin=pinB4)
        self.ch2 = tim.channel(2, pyb.Timer.PWM, pin=pinB5)
        self.ch1.pulse_width_percent(0)
        self.ch2.pulse_width_percent(0)
        self.pinA15.low()
        print('Created a Motor Driver Object')
        
    def enable(self):
        print('Enabling Motor')
        self.pinA15.high()
        
    def disable(self):
        print('Disabling Motor')
        self.pinA15.low()
        
    def set_duty(self, duty):
        '''
        @param duty   A scalar value between -100 and 100 to control the % pulse width modulation
        '''
        if duty < 0:
            self.ch1.pulse_width_percent(abs(duty))
            self.ch2.pulse_width_percent(0)
        if duty >= 0:
            self.ch2.pulse_width_percent(duty)
            self.ch1.pulse_width_percent(0)

