'''
@file Lab7PCUIMain.py
@brief The main file calling the \ref Lab7PCUI.py task on the python kernal.
@author Kai Quizon
'''

import serial
import time
from matplotlib import pyplot
import numpy as np
from Lab7PCUI import Lab7PCUI

task = Lab7PCUI(0.1)

## Run Task
while True:
    task.run()

