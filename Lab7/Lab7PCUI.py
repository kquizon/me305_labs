'''
@file Lab7PCUI.py
@brief This file contains a class governing the PC User Interface for Lab 7 Motor Controller loaded into \ref Lab7PCUIMain.py.
@author Kai Quizon
'''

import serial
import time
from matplotlib import pyplot as plt

#initiate serial port
ser = serial.Serial(port='COM10', baudrate = 115273,timeout=1)

class Lab7PCUI:
    '''
    @brief PC User Interface task for Lab 7 variable motor control.
    
    @details An object of this class initializes and runs the user interface corresponding
    to Nucleo L476 Dev Board sensor data collection. The only user interface
    is the desired proportionality constant for closed loop feedback control.
    COM ports, baudrate, and timeouts are pre-perscribed.
    '''
    
    ## Distribution
    S0_Distribution             = 0
    
    ## Data Processing (State 1)
    S1_Processing               = 1
    

    
    def __init__(self, interval):
        '''
        @param interval Interval describes the interval at which the task will loop.
        '''
        
        ## Serial port
        self.ser = ser
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_Distribution
        
            
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## Initial control parameters
        self.Kp = input('Please input a Kp: ')
        
        
        
        
    def run(self):
        '''
        Runs one iteration of the task.
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            ## Check control parameters and send to Nuke
            if(self.state == self.S0_Distribution):
               
                ## Handle invalid Kps
                if float(self.Kp) <= 0:
                    self.Kp = input('Please input a positive, nonzero Kp: ')
                ## Write control parameters to serial port
                Kptransmit = str(self.Kp)+'\n'
                self.ser.write(Kptransmit.encode())
                
                ## Transition to waiting/processing state
                self.transitionTo(self.S1_Processing)
                
                
            ## Wait for data to be sent back, process, and plot
            elif(self.state == self.S1_Processing):
                
                ## Wait for return serial bus and read
                if self.ser.in_waiting != 0:
                    
                    ## Begin Data Transfer
                    J = ser.readline().decode('ascii')
                    
                    time_stamp = list(ser.readline().decode('ascii').split("array('f'"))
                    vel_act = list(ser.readline().decode('ascii').split("array('f'"))
                    pos_act = list(ser.readline().decode('ascii').split("array('f'"))

                    trunc_vel = list(ser.readline().decode('ascii').split("array('f'"))
                    trunc_pos = list(ser.readline().decode('ascii').split("array('f'"))
                    
                    ## Plot Motor Response
                    fig, axes = plt.subplots(2)
                    fig.suptitle('Motor Response Plots compared to Reference Profiles with Kp = '+str(0.08)+' and J = '+str(J))
                    plt.subplot(2,1,1)
                    plt.plot(time_stamp, vel_act, 'b', time_stamp, trunc_vel, 'r--')
                    plt.xlabel('Time (s)')
                    plt.ylabel('Motor Velocity (RPM)')
                    plt.subplot(2,1,2)
                    plt.plot(time_stamp, pos_act, 'b', time_stamp, trunc_pos, 'r--')
                    plt.xlabel('Time (s)')
                    plt.ylabel('Motor Position (degrees)')
                    
                    
                    ## Creating CSV File
                    #filename = 'MotorVelocity'+time.strftime('%Y%m%d-%H%M%S')
                    #np.savetxt(filename, [time_stamp, motor_velocity], delimiter = ', ')
                    quit()

                                
                
            self.next_time += self.interval    
                
    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState
                      
                
    
    
    
    
    
    
    
    
    
    