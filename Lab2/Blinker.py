'''
@file Blinker.py

This file uses a finite state machine to "blink" a virtual LED.
'''

import time


class Blink:
    '''
    @brief      A finite state machine to blink a virtual LED.
    @details    This class implements a finite state machine to blink a virtual
                LED by printing "LED On" and "LED Off" to the command window.
    '''
    
    ## Constant defining State 0
    S0_OFF   = 0    
    
    ## Constant defining State 1
    S1_ON   = 1    
    
    def __init__(self, interval):
        '''
        @brief            Creates an Blink object.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_OFF
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_OFF):
                print('LED Off')
                self.transitionTo(self.S1_ON)
                
            elif(self.state == self.S1_ON):
                print('LED On')
                self.transitionTo(self.S0_OFF)
                
            # Specifying the next time the task will run
            self.next_time += self.interval                
                
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
             