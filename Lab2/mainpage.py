'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
Welcome! This website hosts documentation related to the repository located 
at:
    https://bitbucket.org/kquizon/me305_labs/src/master/

@section sec_directory Directory
1. \ref page_fibonacci
2. \ref page_Statesys
3. \ref page_Blinker

@page page_fibonacci Fibonacci (Lab 1)

@section page_fib_src Source Code Access
You can find the source code for the fibonacci program here: https://bitbucket.org/kquizon/me305_labs/src/master/Lab1/


@section page_fib_doc Documentation
The documentation for the fibonacci program is located at \ref Lab1Main.py.

@page page_Statesys State System (Elevator--HW 1)

@section page_ss_desc Description
The Elevator State System code allows for the operation of two elevator systems
between two floors. Each floor contains a sensor indidcating whether the 
elevator is located on that floor. The elevator also contains two buttons for 
each floor. These buttons are cleared when the elevator is located at the
corresponding floor. This operation uses state system logic. The state system 
diagram can be viewed below.
@image html StateDiagram.png "Elevator Controls State Diagram" width = 600in

@section page_ss_src Source Code Access
You may access the source code for the Elevator State System program here: https://bitbucket.org/kquizon/me305_labs/src/master/Homework%201/
    
@section page_ss_doc Documentation
The documentation for the elevator state program is located at \ref FSM_Elevator.py

@page page_Blinker Blinking LED (Lab 2)

@section page_Blinker_desc Description
This code runs two tasks simultaneously on the Nucleo L476 Dev Board. One task
prints to the command module whether a "Digital LED" is on or off. This task
simply flips between two states, as shown in teh state diagram below. The
second tasks uses pulse width modulation to control the brightness of a 
physical LED on the Nucleo Dev board. This LEDs brightness varies according
to a sawtooth form. The corresponding state diagram may be viewed below.

Digital LED State Diagram:
@image html DigitalBlink.png "Digital LED State Diagram" width = 600in
    
Physical LED State Diagram:
@image html PhysicalBlink.png "Physical LED State Diagram" width = 600in

@section page_Blinker_src Source Cord Access
You can find the source code for the fibonacci program here: https://bitbucket.org/kquizon/me305_labs/src/master/Lab2/

@section page_Blinker_doc Documentation
The documentation for the elevator state program is located at \ref NucleoBlink.py
'''