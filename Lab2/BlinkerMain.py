'''
@file BlinkerMain.py

@author Kai Quizon
'''

from Blinker import Blink

task1 = Blink(1) #call the blink task where 1 is in the interval time

for N in range(100000000): # effectively while(True):
    # Run the Blink Task
    task1.run()