'''
@file NucleoBlink.py
@brief This is the main file that runs \ref PhysLED.py and \ref Blinker.py

@author Kai Quizon
'''

from PhysLED import PhysBlinker
from Blinker import Blink


task1 = PhysBlinker(1) #create a task that will control the physical LED
task2 = Blink(1) #create a task that will blink the virtual LED

while True:
    task1.run()
    task2.run()