'''
@file PhysLED.py

@author Kai Quizon

@brief This file controls a physical LED using PWM on the Nucleo Board.

@details This file controls a physical LED on the Nucleo dev board. The LED follows
a sawtooth pattern in its brigtness settings as shown below. @image html SawtoothForm.png "Sawtooth Waveform"
'''
import time
import pyb

class PhysBlinker:
    '''
    @brief      A finite state machine to vary the brightness of an LED.
    @details    This class implements a finite state machine to control the
                brightness of a physical LED on the Nucleo dev board. The LED 
                follows a sawtooth wave form in its brightness settings as 
                shown below. @image html SawtoothForm.png "Sawtooth Waveform"
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT         = 0    
    
    ## Constant defining State 1
    S1_ZERO         = 1    
    

    def __init__(self, interval):
        '''
        @brief            Creates an PhysBlinker object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        self.val = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## Initialize LED settings
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        tim2 = pyb.Timer(2, freq = 20000)
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

        self.tick = t2ch1
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''

        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_ZERO)
                self.tick.pulse_width_percent(0)
                print('Initializing')


            elif(self.state == self.S1_ZERO):
                # Run State 1 Code
                self.tick.pulse_width_percent(self.val)
                #print(self.val)
                self.inc()
                
            else:
                # Invalid state code (error handling)
                pass
            
            # Specifying the next time the task will run
            self.next_time += self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def inc(self):
        '''
        @brief      Updates increment to next value
        '''
        self.val += 10  
        if self.val == 110:
            self.val =0