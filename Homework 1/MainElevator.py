'''
@file MainElevator.py
'''

from FSM_Elevator import Button, MotorDriver, ElevatorOperator


# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
Button1 = Button('PB6')
Button2 = Button('PB7')
Floor1sens = Button('PB8')
Floor2sens = Button('PB9')
Motor = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = ElevatorOperator(0.1, Button1, Button2, Floor1sens, Floor2sens, Motor)
task2 = ElevatorOperator(0.1, Button1, Button2, Floor1sens, Floor2sens, Motor)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()
