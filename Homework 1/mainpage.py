'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
Welcome! This website hosts documentation related to the repository located 
at:
    https://bitbucket.org/kquizon/me305_labs/src/master/

@section sec_directory Directory
1. \ref page_fibonacci
2. \ref page_Statesys

@page page_fibonacci Fibonacci (Lab 1)

@section page_fib_src Source Code Access
You can find the source code for the fibonacci program here: https://bitbucket.org/kquizon/me305_labs/src/master/


@section page_fib_doc Documentation
The documentation for the fibonacci program is located at \ref Lab1Main.py.

@page page_Statesys State System (Elevator--HW 1)

@section page_ss_desc Description
The Elevator State System code allows for the operation of two elevator systems
between two floors. Each floor contains a sensor indidcating whether the 
elevator is located on that floor. The elevator also contains two buttons for 
each floor. These buttons are cleared when the elevator is located at the
corresponding floor. This operation uses state system logic. The state system 
diagram can be viewed below.
@image html StateDiagram.png

@section page_ss_src Source Code Access
You may access the source code for the Elevator State System program here: https://bitbucket.org/kquizon/me305_labs/src/master/
    
@section page_ss_doc Documentation
The documentation for the elevator state program is located at \ref FSM_Elevator.py
'''