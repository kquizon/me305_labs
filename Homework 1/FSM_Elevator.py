'''
@file FSM_Elevator.py

This file controls an elevator operating between two floors by implementing a 
finite state machine model.

The user has two buttons to select a floor.

There are sensors at each floor indicating whether the elevator is on the
floor or not.
'''

from random import choice
import time

class ElevatorOperator:
    '''
    @brief      A finite state machine to control an elevator between two floors.
    @details    This class implements a finite state machine to control the
                operation of two elevators moving asychronously between two
                floors.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT   = 0    
    
    ## Constant defining State 1
    S1_DOWN   = 1    
    
    ## Constant defining State 2
    S2_UP     = 2    
    
    ## Constant defining State 3
    S3_STOP_1 = 3    
    
    ## Constant defining State 4
    S4_STOP_2 = 4
    
    
    def __init__(self, interval, Button1, Button2, Floor1sens, Floor2sens, Motor):
        '''
        @brief            Creates an ElevatorOperator object.
        @param Button1    An object from class Button representing the floor 1 button.
        @param Button2    An object from class Button representing the floor 2 button.        
        @param Floor1sens  An object from class Button representing the floor 1 sensor.
        @param Floor2sens An object from class Button representing the floor 2 sensor.
        @param Motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for the go command
        self.Button1 = Button1
        
        ## The button object used for the left limit
        self.Button2 = Button2
        
        ## The button object used for the right limit
        self.Floor1sens = Floor1sens
        
        ## The button object used for the right limit
        self.Floor2sens = Floor2sens
        
        ## The motor object "wiping" the wipers
        self.Motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_DOWN)
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
                self.Motor.Down()
                
            elif(self.state == self.S1_DOWN):
                # Run State 1 Code
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
                if(self.Floor1sens.getButtonState()):
                    self.transitionTo(self.S3_STOP_1)
                    self.Motor.Stop()
            
            elif(self.state == self.S2_UP):
                # Run State 2 Code
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
                if(self.Floor1sens.getButtonState()):
                    self.transitionTo(self.S4_STOP_2)
                    self.Motor.Stop()
                    
            elif(self.state == self.S3_STOP_1):
                # Transition to state 2 if the second floor button is pressed
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
                if(self.Button2.getButtonState()):
                    self.transitionTo(self.S2_UP)
                    self.Motor.Up()
                

                
            
            elif(self.state == self.S4_STOP_2):
                # Run State 4 Code
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
                if(self.Button1.getButtonState()):
                    self.transitionTo(self.S1_DOWN)
                    self.Motor.Down()
                
                
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time += self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the motor forward
        '''
        motor = 1
        print('Motor moving Up (motor=1)')
    
    def Down(self):
        '''
        @brief Moves the motor forward
        '''
        motor = 2
        print('Motor moving down (motor=2)')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        motor = 0
        print('Motor Stopped (motor=0)')



























