'''
@file       Lab1Main.py
@brief      A file containing a function to calculate a Fibonacci Number.
@details    A file containing a function that calculates the Fibonacci Number 
            corresponding to the input idx. If the input idx is not a positive
            integer, the function returns a corresponding error message.
@author     Kai Quizon
'''
import sys
def fib(idx):

    ''' 
    @brief fib is a function that calculates the Fibonacci number for idx.
    @details fib calculates the Fibonacci number at the index idx if idx is 
             a positive, real integer. Otherwise, fib asks for a positive, real
             integer input.
    @param idx An integer specifiying the index of the desired Fibonacci
    number.'''
    
    #Check validity of the index

    while True:
        try:
            fib0=0
            fib1=1
            idx = int(idx)
            if idx < 0: #Ensure index is positive
                print('Invalid index. Please input a real, positive integer')
            elif idx == 0: #return fib(0) if idx = 0
                print('Calculating Fibonacci number at ' 'index n={:}.'.format(idx))
                print(fib0)
            elif idx == 1: #return fib(1) if idx = 1
                print('Calculating Fibonacci number at ' 'index n={:}.'.format(idx))
                print(fib1)
            else: #for all other valid idx, solve for appropriate fib
                print('Calculating Fibonacci number at ' 'index n={:}.'.format(idx))
                for n in range(2,idx+1): #set calculation range
                    fib3 = fib0+fib1 #loop to calculate without storing seq.
                    fib0=fib1
                    fib1=fib3
                print(fib3)
            idx=input('Enter another index or enter Exit to Exit: ')
        except NameError:
            print('end')
            raise Exception('exit')
        except ValueError:
            if idx == 'Exit' or 'exit':
                print('Goodbye!')
                raise Exception('Exit')
            else:
                print('Please enter a positive integer index.')
                idx=input('Enter another index or enter Exit to Exit: ')

if __name__ == '__main__': 
    fib(17)
