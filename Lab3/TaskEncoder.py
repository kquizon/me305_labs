'''
@file TaskEncoder.py
@brief Encoder task updating encoder readout and accepting user commands (modified from code originally published by Charlie Revfam)
@author Kai Quizon
'''

import enc_shares
import utime
from Encoder import Encoder

class TaskEncoder:
    '''
    Encoder task.
    
    An object of this class reads values from a motor encoder or updates them as dictated by the user inputs.   
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_CMD     = 1

    def __init__(self, taskNum, interval, pin1, pin2, timer, Encoder, dbg=True):
        '''
        Creates an Encoder task object.
        @param taskNum A number to identify the task.
        @param interval An integer number of microseconds between desired runs of the task
        @param pin1 The first pin on the Nucleo board the encoder is connected to (ex: 'A6')
        @param pin2 The second pin on the Nucleo board the encoder is connected to (ex: 'A7')
        @param timer The appropriate timer corresponding to the selected pins, as demanded by the Nucleo documentation
        @param Encoder The Encoder class on which the Encoder task is to operate
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
            
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        if self.dbg:
            print('Created encoder task')
        
        self.Encoder = Encoder #store class copy
        
        self.enc_pos = 0 #initialize encoder at zero
        self.enc_prev = 0 #initialize previous encoder value at zero
        self.tim = Encoder.tim

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            Encoder.update(self)
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                self.printTrace()
                # Run State 1 Code
                if enc_shares.cmd == 112:
                    enc_shares.resp = 1
                    enc_shares.printval = int(Encoder.get_position(self))
                    enc_shares.cmd = None
                elif enc_shares.cmd == 122:
                    enc_shares.resp = 1
                    enc_shares.printval = 'Encoder is Zeroed'
                    self.Encoder.set_position(0)

                    enc_shares.cmd = None
                elif enc_shares.cmd == 100:
                    enc_shares.resp = 1
                    enc_shares.printval = int(Encoder.get_delta(self))
                    enc_shares.cmd = None
                else:
                    enc_shares.printval = 'Please input a valid command.esssss'
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        @param newState The new state for the object to assume.
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
            
            
                                                for n in range(len(self.DataCollection)):
                                        #print to putty terminal (debugging)
                                        #print('{:},{:}'.format(self.Time[n],self.DataCollection[n]))
