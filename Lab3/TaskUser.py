'''
@file TaskUser.py
@brief User interface task talking to \ref TaskEncoder.py (modified from code originally published by Charlie Revfam)
@author Kai Quizon
'''

import enc_shares
import utime
from pyb import UART

class TaskUser:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    input. Each user input is checked. If it is a valid user input as defined
    by the user input guides (below), the appropriate value is returned.    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for input state
    S1_WAIT_FOR_INPUT    = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2

    def __init__(self, taskNum, interval, dbg=True):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
        if self.dbg:
            print('Created user interface task')
        
        print('''
              Allowable User Inputs:
                  z: Zero the Encoder Position
                  p: Print the Current Encoder Position
                  d: Print the most recent Delta Value
              ***NOTE: User Inputs are case sensitive.    
              ''')

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_INPUT)
            
            elif(self.state == self.S1_WAIT_FOR_INPUT):
                self.printTrace()
                # Run State 1 Code
                if self.ser.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    enc_shares.cmd = self.ser.readchar()
                    
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                self.printTrace()
                # Run State 2 Code
                if enc_shares.resp:
                    self.transitionTo(self.S1_WAIT_FOR_INPUT)
                    print(enc_shares.printval)
                    enc_shares.resp = None
                    enc_shares.printval = None
                    
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)