'''
@file Lab3main.py
@brief Main program runs all the tasks round-robin (adapted from code published by Charlie Revfam)
@details This program calls a User Interface from \ref TaskUser.py and an
         instance of the \ref Encoder.py task in \ref TaskEncoder.py.
         \ref TaskUser.py and \ref TaskEncoder.py share data through \ref enc_shares.py
@author Kai Quizon 
'''

import enc_shares
from TaskUser import TaskUser
from TaskEncoder import TaskEncoder
from Encoder import Encoder

Encoder = Encoder('A6','A7',3)

## User interface task, works with serial port
task0 = TaskUser  (0, 1_00, dbg=False)

## Encoder Task interacts with Motor Encoder hardware to read and write position
task1 = TaskEncoder(1, 1_00, 'A6', 'A7', 3, Encoder, dbg=False)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()