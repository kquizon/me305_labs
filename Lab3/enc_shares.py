'''
@file shares.py
@brief A container for all the inter-task variables between \ref TaskEncoder.py and \ref TaskUser.py  (modified from code by Charlie Revfam)
@author Kai Quizon
'''

## The command character sent from the user interface task to the cipher task
cmd     = None

## The response from the cipher task after encoding
resp    = None

## The value to be printed
printval = None